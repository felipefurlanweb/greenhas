(function () {
    'use strict';
    angular
        .module('app')
        .factory('CulturaService', CulturaService);

    CulturaService.$inject = ['$q', 'SettingsHelper', '$timeout', '$http'];
    function CulturaService($q, SettingsHelper, $timeout, $http) {

          var service = {
              getAll: getAll,
              getById: getById
          };

          return service;

          function getById(idCultura){
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "getCulturaById", idCultura: idCultura}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  if(response.data.length > 0){
                    for(var i = 0; i <= response.data.length - 1; i++){
                      response.data[i].imagem = SettingsHelper.BaseUrl + 'admin/images/' + response.data[i].imagem;
                    }
                  }
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }


          function getAll() {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "getAllCulturas"}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  if(response.data.length > 0){
                    for(var i = 0; i <= response.data.length - 1; i++){
                      response.data[i].imagem = SettingsHelper.BaseUrl + 'admin/images/' + response.data[i].imagem;
                    }
                  }
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

    }

}());