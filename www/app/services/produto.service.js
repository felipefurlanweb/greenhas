(function () {
    'use strict';
    angular
        .module('app')
        .factory('ProdutoService', ProdutoService);

    ProdutoService.$inject = ['$q', 'SettingsHelper', '$timeout', '$http'];
    function ProdutoService($q, SettingsHelper, $timeout, $http) {

          var service = {
              getAll: getAll,
              getAllByCultura: getAllByCultura,
              getTratamentosByIdProduto: getTratamentosByIdProduto,
              getFolhagensByIdProduto: getFolhagensByIdProduto,
              getProdutoEtapaByIdProduto: getProdutoEtapaByIdProduto
          };

          return service;


          function getProdutoEtapaByIdProduto(idProduto, idTratamento){
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "getProdutoEtapaByIdProduto", idProduto: idProduto, idTratamento: idTratamento}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise; 
          }

          function getFolhagensByIdProduto(idProduto){
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "getFolhagensByIdProduto", idProduto: idProduto}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise; 
          }

          function getTratamentosByIdProduto(idProduto){
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "getTratamentosByIdProduto", idProduto: idProduto}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

          function getAll(id) {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "getAllProdutos"}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

          function getAllByCultura(id) {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "getAllProdutosByCultura", idCultura: id}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

    }

}());