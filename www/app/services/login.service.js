﻿(function () {
    'use strict';
    angular
        .module('app')
        .factory('LoginService', LoginService);

    LoginService.$inject = ['$http', '$q', 'localStorageService', 'SettingsHelper'];
    function LoginService($http, $q, localStorageService, SettingsHelper) {
        var service = {
            login: login,
            save: save,
            logout: logout,
            isAuthenticate: isAuthenticate,
            getLoginData: getLoginData
        };

        var auth = defaultAuth();

        return service;

        //////////////////////////////////////////////////////////////////////////

        function login(model) {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "login", email: model.email, senha: model.senha}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
        }


        function save(model) {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "clienteAdd", 
                  nome: model.nome,
                  email: model.email,
                  profissao: model.profissao,
                  celular: model.celular,
                  pais: model.pais,
                  estado: model.estado,
                  cidade: model.cidade,
                  cep: model.cep,
                  senha: model.senha
                  }),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
        }


        function isAuthenticate() {
            return auth.isAuth;
        }

        function logout() {
            localStorageService.remove(SettingsHelper.StorageName);
            auth.isAuth = false;
        }

        function getLoginData() {
            var data = localStorageService.get(SettingsHelper.StorageName);
            if (data) {
                auth = data;
            }
        }

        //////////////////////////////////////////////////////////////////////////

        function defaultAuth() {
            return {
                isAuth: false,
                username: ''
            }
        }
    }

}());