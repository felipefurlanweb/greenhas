(function () {
    'use strict';
    angular
        .module('app')
        .factory('ProdutoIndividualService', ProdutoIndividualService);

    ProdutoIndividualService.$inject = ['$q', 'SettingsHelper', '$timeout', '$http', 'localStorageService'];
    function ProdutoIndividualService($q, SettingsHelper, $timeout, $http, localStorageService) {

          var service = {
              getAll: getAll,
              getById: getById,
              sendEmail: sendEmail
          };

          return service;

          function sendEmail(obj) {
              var promise = null;
              var idUser = localStorageService.get("greenhas");
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "sendEmail", nomeProduto: obj.produto.nome, mensagem: obj.mensagem, idUser: idUser}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

          function getAll(id) {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "getAllProdutosIndividual"}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

          function getById(id) {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "getProdutosIndividualById", idProdutoIndividual: id}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

    }

}());