﻿(function () {
    'use strict';
    angular
        .module('app')
        .factory('UsuarioService', UsuarioService);

    UsuarioService.$inject = ['$q', 'SettingsHelper', '$timeout', '$http', 'localStorageService'];
    function UsuarioService($q, SettingsHelper, $timeout, $http, localStorageService) {
        var service = {
            sendEmail: sendEmail
        };

        return service;

        //////////////////////////////////////////////////////////////////////////

        function sendEmail(obj){
              var promise = null;
              var idUser = localStorageService.get("greenhas");
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "app/www/server/form.php",
                  data: $.param({ id: "sendEmailContato", mensagem: obj.mensagem, imagem: obj.imagem, idUser: idUser}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
        }

    }

}());