﻿(function () {
    'use strict';
    angular
        .module('app')
        .factory('SettingsHelper', SettingsHelper);

    SettingsHelper.$inject = [];
    function SettingsHelper() {
        var service = {
            //BaseUrl: 'http://localhost/greenhas/',
            //BaseUrl: 'http://ffdevweb.com.br/projetos/greenhas/',
            BaseUrl: 'http://greenhas.com.br/app/',
            StorageName: 'greenhas',
            CurrentLanguage: 'pt-BR'
        };

        return service;
    }

}());