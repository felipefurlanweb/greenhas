﻿(function () {
    'use strict';
    angular
        .module('app')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('home');

        $stateProvider
            .state('login',
            {
                url: '/login',
                templateUrl: "app/modules/login/login.html",
                controller: "LoginController",
                data: { pageTitle: 'Página de Login' },
                resolve: {

                }
            })
            .state('home',
            {
                url: '/home',
                templateUrl: "app/modules/home/home.html",
                controller: "HomeController",
                data: { pageTitle: 'Home' },
                resolve: {

                }
            })

    }

}());
