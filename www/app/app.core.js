﻿(function () {
    'use strict';
    angular
        .module('app.core', [
            'ui.router',
            'angularValidator',
            'LocalStorageModule',
            'ngSanitize',
            'br.cidades.estados'
        ]);
}());