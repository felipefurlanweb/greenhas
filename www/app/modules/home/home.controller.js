﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('HomeController', HomeController)

    HomeController.$inject = ['$rootScope', '$scope', 'CulturaService', 'ProdutoService', '$filter', 
    'SettingsHelper', 'ProdutoIndividualService', 'UsuarioService', 'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function HomeController($rootScope, $scope, CulturaService, ProdutoService, $filter, 
    	SettingsHelper, ProdutoIndividualService, UsuarioService, localStorageService, $state, $ionicPlatform, $ionicHistory) {

	    $ionicPlatform.registerBackButtonAction(function(e) {
	      showBoxHome();
	    }, 100);

		$rootScope.abas = true;
		$rootScope.bkgPicture = false;
		$rootScope.culturaDetails  = false;
		$rootScope.boxProduct  = false;
		$rootScope.boxTratamentos  = false;
		$rootScope.boxInfoProduct  = false;
		$rootScope.catSelect1 = true;
		$rootScope.boxCat1 = true;
		$rootScope.boxInfoProductIndividual  = false;
		$rootScope.boxEmailPedido  = false;
		$rootScope.boxInfoEtapas  = false;
		$scope.boxInfoTratamento = false;
		$scope.boxInfoFolhagem = false;
		
		$scope.idCultura = 0;
		$scope.culturas = [];
		$rootScope.cultura = {};
		$scope.email = {};
		$scope.produto = {};
		$scope.emailContato = {};
		$scope.etapasFenologicas = {};
		$rootScope.produtos = [];
		$scope.tratamentos = [];
		$scope.folhagens = [];
		$scope.produtosIndividual = [];

		$scope.selectCat = selectCat;
		$scope.DesSelectCat = DesSelectCat;
		$scope.hideAllBoxCat = hideAllBoxCat;
		$scope.getAllCulturas = getAllCulturas;
		$scope.selectCultura = selectCultura;
		$scope.showBoxHome = showBoxHome;
		$scope.selectProduto = selectProduto;
		
		$scope.getAllProdutos = getAllProdutos;
		$scope.selectProduto = selectProduto;
		$scope.selectProdutoAba = selectProdutoAba;
		$scope.showBoxHomeAbaProdutos = showBoxHomeAbaProdutos;
		$scope.getAllTratamentos = getAllTratamentos;
		$scope.hideAllBox = hideAllBox;
		$scope.getCulturaById = getCulturaById;
		$scope.showBoxProdutos = showBoxProdutos;
		$scope.selectProdutoIndividual = selectProdutoIndividual;
		$scope.showBoxEmailPedido = showBoxEmailPedido;
		$scope.sendEmail = sendEmail;
		$scope.sendEmailContato = sendEmailContato;
		$scope.selectProdutoEtapa = selectProdutoEtapa;
		$scope.showBoxInfoProduto = showBoxInfoProduto;
		$scope.logout = logout;
		$scope.download = download;
    	$scope.close = close;
    	$scope.goToLogin = goToLogin;

		$scope.activate = activate();

		/////////////////////////////////////////////////////////////////////////////////
		function activate(){
			$scope.getAllCulturas();
			var teste = localStorageService.get("greenhas");
	       	$scope.logado = false;
	        if(teste != null){
	        	$scope.logado = true;
			}
		}
		/////////////////////////////////////////////////////////////////////////////////

	    function close(){
	      navigator.app.exitApp();
	    }

	    function goToLogin(){
            $rootScope.bkgPicture = true;
            $rootScope.bkgPicture2 = false;
            $rootScope.bodyWhite = false;
	    	$state.go("login");
	    }


		/////////////////////////////////////////////////////////////////////////////////

		function download(arq){
			window.open("http://greenhas.com.br/app/admin/files/" +arq);
		}

		function logout(){
			$rootScope.bkgPicture = true;
			$rootScope.bkgPicture2 = false;
			$rootScope.bodyWhite = false;
			$rootScope.loader = false;
			localStorageService.remove("greenhas");
			$state.go("login");
		}

		function showBoxInfoProduto(){
			$scope.hideAllBox();
			$rootScope.boxInfoProduct  = true;
		}

		function selectProdutoEtapa(item, produto){
			$rootScope.loader = true;
			ProdutoService.getProdutoEtapaByIdProduto(produto.id, item.id).then(function(res){
				if(res.data.length > 0){
					$scope.hideAllBox();
					$rootScope.boxInfoEtapas  = true;
					$scope.etapasFenologicas = res.data[0];
				}else{
					alert("Nenhum registro cadastrado.");
				}
				$rootScope.loader = false;
			}).catch(function(err){
				$rootScope.loader = false;
				console.log(err);
			});
		}

		function sendEmailContato(){
			$rootScope.loader = true;
			UsuarioService.sendEmail($scope.emailContato).then(function(res){
				if(res.data.length > 0){
					alert("Mensagem enviada com sucesso!");
					$scope.emailContato = {};
				}
				$rootScope.loader = false;
			}).catch(function(err){
				$rootScope.loader = false;
				console.log(err);
			});
		}

		function sendEmail(){
			$scope.email.produto = $scope.produtoIndividual[0];
			ProdutoIndividualService.sendEmail($scope.email).then(function(res){
				if(res.data.length > 0){
					alert("Mensagem enviada com sucesso!");
					$rootScope.boxEmailPedido  = false;
				}
			}).catch(function(err){
				console.log(err);
			});
		}

		function showBoxEmailPedido(){
			$rootScope.boxEmailPedido  = true;
		}

		function selectProdutoIndividual(item){
			$rootScope.loader = true;
			ProdutoIndividualService.getById(item.id).then(function(res){
				if(res.data.length > 0){
					$scope.produtoIndividual = res.data;
					$scope.produtoIndividual[0].imagem = SettingsHelper.BaseUrl+'/admin/images/' + $scope.produtoIndividual[0].imagem;
					$scope.hideAllBox();
					$rootScope.boxInfoProductIndividual  = true;
				}
				$rootScope.loader = false;
			}).catch(function(err){
				console.log(err);
				$rootScope.loader = false;
			});
		}

		function showBoxProdutos(){
			$scope.hideAllBox();
			$rootScope.abas = true;
			$scope.catSelect2 = true;
			$scope.boxCat2 = true;
			$scope.getAllProdutos();
		}

		function getCulturaById(idCultura){
			CulturaService.getById(idCultura).then(function(res){
				if(res.data.length > 0){
					$rootScope.cultura = res.data[0];
				}
			}).catch(function(err){
				console.log(err);
			});
		}

		function hideAllBox(){
			$rootScope.abas = false;
			$rootScope.boxEmailPedido  = false;
			$rootScope.bkgPicture = false;
			$rootScope.culturaDetails  = false;
			$rootScope.boxProduct  = false;
			$rootScope.boxTratamentos  = false;
			$rootScope.boxInfoProduct  = false;
			$rootScope.catSelect1 = false;
			$rootScope.boxCat1 = false;
			$rootScope.boxInfoProductIndividual  = false;
			$rootScope.boxInfoEtapas  = false;
		}

		function getAllTratamentos(id){
			var aux = 1;
			$scope.produto = $filter("filter")($scope.produtos, {id: id},true)[0];
			ProdutoService.getTratamentosByIdProduto(id).then(function(res){
				if(res.data.length > 0){
					$scope.tratamentos = res.data;
					$rootScope.boxTratamentos  = true;
				}else{
					aux = 0;
				}
				ProdutoService.getFolhagensByIdProduto(id).then(function(res){
					if(res.data.length > 0){
						$scope.folhagens = res.data;
					}else{
						aux = 0;
					}
					if(aux == 0){
						alert("Este produto não tem informação");
						$scope.showBoxHomeAbaProdutos();
					}
				}).catch(function(err){
					console.log(err);
				});
			}).catch(function(err){
				console.log(err);
			});
		}

		function showBoxHomeAbaProdutos(op){
			$scope.hideAllBox();
			$rootScope.abas = true;
			$scope.catSelect2 = true;
			$scope.boxCat2 = true;
		}

		function selectProduto(item){
			$scope.produto = item;
			ProdutoService.getTratamentosByIdProduto(item.id).then(function(res){
				if(res.data.length > 0){
					$scope.tratamentos = res.data;
					$scope.boxInfoTratamento = true;
				}else{
					$scope.boxInfoTratamento = false;
				}
				ProdutoService.getFolhagensByIdProduto(item.id).then(function(res){
					if(res.data.length > 0){
						$scope.folhagens = res.data;
						$scope.boxInfoFolhagem = true;
					}else{
						$scope.boxInfoFolhagem = false;
					}
					CulturaService.getById(item.idCultura).then(function(res){
						if(res.data.length > 0){
							$rootScope.cultura = res.data[0];
						}
						$rootScope.boxInfoProduct  = true;
					}).catch(function(err){
						console.log(err);
					});
				}).catch(function(err){
					console.log(err);
				});
			}).catch(function(err){
				console.log(err);
			});
		}

		function selectProdutoAba(item){
			$rootScope.abas = false;
			$rootScope.boxProduct  = false;
			$scope.getAllTratamentos(item.id);
			$scope.getCulturaById(item.idCultura);
		}

		function getAllProdutos(){
			$rootScope.loader = true;
			ProdutoIndividualService.getAll().then(function(res){
				if(res.data.length > 0){
					$scope.produtosIndividual = res.data;
				}
				$rootScope.loader = false;
			}).catch(function(err){
				console.log(err);
				$rootScope.loader = false;
			});
		}

		function showBoxHome(){
			$scope.hideAllBox();
			$rootScope.abas = true;
			$rootScope.catSelect1 = true;
			$rootScope.boxCat1 = true;
			$rootScope.produtos = [];
			$rootScope.cultura = {};
			getAllCulturas();
		}

		function selectCultura(item){
			$rootScope.loader = true;
			ProdutoService.getAllByCultura(item.id).then(function(res){
				if(res.data.length > 0){
					$rootScope.produtos = res.data;
					$rootScope.cultura = item;
					$rootScope.abas = false;
					$rootScope.boxCat1 = false;
					$scope.selectProduto($scope.produtos[0]);
					$rootScope.pageNow = "infoCultura";
					$rootScope.pageBack = "cultura";
				}else{
					alert("Não existe produto para essa cultura");
				}
				$rootScope.loader = false;
			}).catch(function(err){
				console.log(err);
				$rootScope.loader = false;
			});
		}

		function getAllCulturas(){
			CulturaService.getAll().then(function(res){
				if(res.data.length > 0){
					$scope.culturas = res.data;
				}
			}).catch(function(err){
				console.log(err);
			});
		}

		function hideAllBoxCat(){
			$rootScope.boxCat1 = false;
			$scope.boxCat2 = false;
			$scope.boxCat3 = false;
		}


		function DesSelectCat(){
			$rootScope.catSelect1 = false;
			$scope.catSelect2 = false;
			$scope.catSelect3 = false;
		}

		function selectCat(id){
			DesSelectCat();
			hideAllBoxCat();
			if(id == 1){
				$rootScope.catSelect1 = true;
				$rootScope.boxCat1 = true;
				getAllCulturas();
			}else if(id == 2){
				$scope.catSelect2 = true;
				$scope.boxCat2 = true;
				$scope.getAllProdutos();
			}else{
				$scope.catSelect3 = true;
				$scope.boxCat3 = true;
				$("textarea").summernote();
			}
		}

    }

}());