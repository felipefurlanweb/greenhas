﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('LoginController', LoginController)

    LoginController.$inject = ['$state','$rootScope', '$scope', 'brCidadesEstados', 
    'LoginService', 'localStorageService'];
    function LoginController($state, $rootScope, $scope, brCidadesEstados, 
        LoginService, localStorageService, $ionicPlatform) {

        $scope.model = {};
        $scope.boxLogin = true;
        $scope.boxCadastro = false;
        $scope.showBoxCadastro = showBoxCadastro;
        $scope.showBoxLogin = showBoxLogin;
        $scope.buscarCidadesPorSigla = buscarCidadesPorSigla;
        $scope.save = save;
        $scope.login = login;
        $scope.goBoxHome = goBoxHome;
        $scope.estados = [];
        $scope.cidades = [];

        ///////////////////////////////////////////////////////////////////////////

        activate();

        //////////////////////////////////////////////////////////////////////////

        function goBoxHome(){
            $rootScope.bkgPicture = false;
            $rootScope.bkgPicture2 = false;
            $rootScope.bodyWhite = true;
            $state.go("home");
        }

        function activate() {
            //localStorageService.remove("greenhas");
            var teste = localStorageService.get("greenhas");
            if(teste != null){
                $state.go("home");
            }
        }

        //////////////////////////////////////////////////////////////////////////

        function login(){
            LoginService.login($scope.login).then(function(res){
                if(res.data.length > 0){
                    $rootScope.bkgPicture = false;
                    $rootScope.bkgPicture2 = false;
                    $rootScope.bodyWhite = true;
                    localStorageService.set("greenhas", res.data[0].id);
                    $state.go("home");
                }else{
                    alert("Nenhum registro encontrado");
                }
            }).catch(function(err){
                console.log(err);
            })
        }

        function showBoxCadastro(){
            $rootScope.bkgPicture = false;
            $rootScope.bkgPicture2 = true;
            $scope.boxLogin = false;
            $scope.boxCadastro = true;
            $scope.estados = brCidadesEstados.estados;
        }

        function showBoxLogin(){
            $rootScope.bkgPicture = true;
            $rootScope.bkgPicture2 = false;
            $scope.boxLogin = true;
            $scope.boxCadastro = false;
        }

        function buscarCidadesPorSigla(){
            $scope.cidades = brCidadesEstados.buscarCidadesPorSigla($scope.model.estado);
        }

        function save(){
            LoginService.save($scope.model).then(function(res){
                console.log(res);
                if (res.data == '1'){
                    alert("Email já cadastrado, informe outro.");
                }else if(res.data.length > 0){
                    $rootScope.bkgPicture = false;
                    $rootScope.bkgPicture2 = false;
                    $rootScope.bodyWhite = true;
                    localStorageService.set("greenhas", res.data[0].id);
                    $state.go("home");
                }else{
                    alert("Algo deu errado, tente novamente");
                }
            }).catch(function(err){
                console.log(err);
            })
        }

    }

}());
