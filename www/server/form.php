<?php 
	header('Access-Control-Allow-Origin: *');
	include_once 'banco.php';

	$id = $_POST["id"];
	//$id = $_GET["id"];
	//var_dump($post);

	switch ($id) {

		case 'login':
			login();
			break;

		case 'getAllProdutos':
			getAllProdutos();
			break;

		case 'getAllCulturas':
			getAllCulturas();
			break;

		case 'getAllProdutosByCultura':
			getAllProdutosByCultura();
			break;

		case 'getTratamentosByIdProduto':
			getTratamentosByIdProduto();
			break;

		case 'getCulturaById':
			getCulturaById();
			break;

		case 'getFolhagensByIdProduto':
			getFolhagensByIdProduto();
			break;

		case 'getAllProdutosIndividual':
			getAllProdutosIndividual();
			break;
		case 'getProdutosIndividualById':
			getProdutosIndividualById();
			break;

		case 'getProdutoEtapaByIdProduto':
			getProdutoEtapaByIdProduto();
			break;



		case 'sendEmail':
			sendEmail();
			break;
		case 'sendEmailContato':
			sendEmailContato();
			break;
		case 'clienteAdd':
			clienteAdd();
			break;

		case 'usuarioAdd':
			usuarioAdd();
			break;

		default:
			# code...
			break;
	}
 
 	/////////////////////////////////////////////////////////////////////////////////////

 	function getProdutoEtapaByIdProduto(){
		$idProduto = $_POST["idProduto"];
		$idTratamento = $_POST["idTratamento"];
		$query = "SELECT * FROM produtos_etapas WHERE idProduto = $idProduto AND idFolhagem = $idTratamento";
		$sth = mysql_query($query);
		$numrows = mysql_num_rows($sth);
		if ($numrows > 0) {
			$rows = array();
			while($r = mysql_fetch_assoc($sth)) {
			    $rows[] = $r;
			}
			echo json_encode($rows);
		}else{
			$query = "SELECT * FROM produtos_etapas WHERE idProduto = $idProduto AND idTratamento = $idTratamento";
			$sth = mysql_query($query);
			$rows = array();
			while($r = mysql_fetch_assoc($sth)) {
			    $rows[] = $r;
			}
			echo json_encode($rows);
		}
		

 	}

	function clienteAdd(){
		$nome = $_POST["nome"];
		$email = $_POST["email"];
		$profissao = $_POST["profissao"];
		$celular = $_POST["celular"];
		$estado = $_POST["estado"];
		$cidade = $_POST["cidade"];
		$cep = $_POST["cep"];
		$senha = $_POST["senha"];
		$senha = md5($senha);
		$query = "SELECT * FROM clientes WHERE email = '$email'";
		$query = mysql_query($query);
		$numrows = mysql_num_rows($query);
		if ($numrows > 0) {
			$rows = array();
			while($r = mysql_fetch_assoc($query)) {
			    $rows[] = $r;
			}
			echo json_encode(1);
			die();
		}
		$query = "INSERT INTO clientes
		(
		nome,
		email,
		profissao,
		celular,
		estado,
		cidade,
		cep,
		senha
		)VALUES(
		'%s',
		'%s',
		'%s',
		'%s',
		'%s',
		'%s',
		'%s',
		'%s'
		)";
		$query = sprintf($query,
		addslashes($nome),
		addslashes($email),
		addslashes($profissao),
		addslashes($celular),
		addslashes($estado),
		addslashes($cidade),
		addslashes($cep),
		addslashes($senha));
		$sth = mysql_query($query);
		$query = "SELECT * FROM clientes ORDER BY data DESC LIMIT 1";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function sendEmailContato(){
		$imagem = $_POST["imagem"];
		$mensagem = $_POST["mensagem"];
		$idUser = $_POST["idUser"];
		$query = "INSERT INTO emailcontato (mensagem, imagem, idUser) VALUES ('%s', '%s', $idUser)";
		$query = sprintf($query, addslashes($mensagem), addslashes($imagem));
		$sth = mysql_query($query);
        $modelo = '
        <table style="width: 980px;margin: 0 auto;padding: 5px;  font-family: sans-serif;">
            <caption style="background: #3c763d; color: #FFF;padding: 20px;margin: 20px 0 10px 0;">
                Email de contato
            </caption>
            <tr>
                <td><b>Imagem</b></td>
                <td>'.$imagem.'</td>
            </tr>
            <tr>
                <td><b>Mensagem</b></td>
                <td>'.$mensagem.'</td>
            </tr>
        </table>
        ';
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
        $headers .= "From: Greenhas <sac@greenhas.com.br>\r\n";
                         
        $mail1 = mail('sac@greenhas.com.br','Email de contato do Aplicativo Green Has', $modelo, $headers);
        $mail2 = mail('felipefurlanweb@gmail.com','Email de contato do Aplicativo Green Has', $modelo, $headers);

		$query = "SELECT * FROM emailcontato ORDER BY data DESC LIMIT 1";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function sendEmail(){
		$nomeProduto = $_POST["nomeProduto"];
		$mensagem = $_POST["mensagem"];
		$idUser = $_POST["idUser"];
		$query = "INSERT INTO emails (mensagem, nomeProduto, idUser) VALUES ('%s', '%s', $idUser)";
		$query = sprintf($query, addslashes($mensagem), addslashes($nomeProduto));
        $modelo = '
        <table style="width: 980px;margin: 0 auto;padding: 5px;  font-family: sans-serif;">
            <caption style="background: #3c763d; color: #FFF;padding: 20px;margin: 20px 0 10px 0;">
                Email de contato
            </caption>
            <tr>
                <td><b>Nome do Produto</b></td>
                <td>'.$nomeProduto.'</td>
            </tr>
            <tr>
                <td><b>Mensagem</b></td>
                <td>'.$mensagem.'</td>
            </tr>
        </table>
        ';
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
        $headers .= "From: Greenhas <sac@greenhas.com.br>\r\n";
                         
        $mail1 = mail('sac@greenhas.com.br','Email de contato do Aplicativo Green Has', $modelo, $headers);
        $mail2 = mail('felipefurlanweb@gmail.com','Email de contato do Aplicativo Green Has', $modelo, $headers);

        	
		$sth = mysql_query($query);
		$query = "SELECT * FROM emails ORDER BY data DESC LIMIT 1";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getProdutosIndividualById(){
		$idProdutoIndividual = $_POST["idProdutoIndividual"];
		$query = "SELECT * FROM produtos_individual WHERE id = $idProdutoIndividual";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllProdutosIndividual(){
		$query = "SELECT * FROM produtos_individual ORDER BY nome ASC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getFolhagensByIdProduto(){
		$idProduto = $_POST["idProduto"];
		$query = "SELECT * FROM folhagens WHERE idProduto = $idProduto";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

 	function getCulturaById(){
		$idCultura = $_POST["idCultura"];
		$query = "SELECT * FROM culturas WHERE id = $idCultura";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
 	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getTratamentosByIdProduto(){
		$idProduto = $_POST["idProduto"];
		$query = "SELECT * FROM tratamentos WHERE idProduto = $idProduto";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllProdutosByCultura(){
		$idCultura = $_POST["idCultura"];
		$query = "SELECT * FROM produtos WHERE idCultura = $idCultura ORDER BY id DESC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function usuarioAdd(){
		$nome = $_POST["nome"];
		$email = $_POST["email"];
		$senha = $_POST["senha"];
		$senha = md5($senha);
		$query = "INSERT INTO usuarios (nome, email, senha) VALUES ('$nome', '$email', '$senha')";
		$query = mysql_query($query);
		$query = "SELECT * FROM usuarios WHERE email = '$email' AND senha = '$senha'";
		$query = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($query)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllCulturas(){
		$query = "SELECT * FROM culturas ORDER BY nome ASC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllProdutos(){
		$query = "SELECT * FROM produtos ORDER BY nomeTratamento";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

	/////////////////////////////////////////////////////////////////////////////////////

	function login(){
		$email = $_POST["email"];
		$senha = $_POST["senha"];
		$senha = md5($senha);
		$query = "SELECT * FROM clientes WHERE email = '$email' AND senha = '$senha'";
		$query = mysql_query($query);
		$numrows = mysql_num_rows($query);
		if ($numrows == 0) {
			$query = "SELECT * FROM administradores WHERE email = '$email' AND senha = '$senha'";
			$query = mysql_query($query);
		}
		if ($query) {
			$rows = array();
			while($r = mysql_fetch_assoc($query)) {
			    $rows[] = $r;
			}
			echo json_encode($rows);
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////

?>